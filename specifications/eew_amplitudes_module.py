#!/usr/bin/env python
# coding: utf-8

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# Integration
class cumtrapz:
    """
    Cumulative trapezoid rule which implicitly 
    connects measurements with a straight line.
    """
    def __init__(self,dt):
        self.fac = dt/2.0
        self.prev_in = 0.0
        self.prev_out = 0.0
        
    def integrate(self,data):
        n = data.size
        out = np.zeros(n)
        if abs(self.prev_in) < 1e-6:
            out[0] = 0.0
        else:
            out[0] = self.prev_out + (data[0]+self.prev_in)*self.fac
        
        for i in xrange(1,n):
            out[i] = out[i-1] + (data[i] + data[i-1])*self.fac
        
        self.prev_in = data[n-1]
        self.prev_out = out[n-1]
        
        return out


# ##### Testing
# In the following test we integrate a sine wave over two cycles and evaluate the errror. The sine wave is further cut into smaller chunks to simulate real-time behaviour.

plt.figure()
f = lambda x: np.sin(x)
f_true = lambda x: -np.cos(x)+1.0
ns = 100
pi2 = 2.0*np.pi
x = np.linspace(0,4*np.pi,ns)
dx = np.mean(np.diff(x))
step = 20
start = 0
end = step
valst = []
valss = []
t = cumtrapz(dx)
while end <= ns:
    x1 = x[start:end]
    y1 = f(x1)
    Yt = t.integrate(y1)
    plt.semilogy(x1/pi2,abs(f_true(x1)-np.array(Yt)),'b-')
    start = end
    end += step
plt.semilogy(x1/pi2,abs(f_true(x1)-np.array(Yt)),'b-',label='Trapezoid rule')
plt.legend(loc='lower left')
plt.ylabel('Error')
plt.xlabel('# of cycles')
plt.title('Integration')


# Differentiation

class onesided:
    """
    This differentiation method assumes measurements
    are connected through straight lines and has an 
    accuracy of O(dx).
    """
    def __init__(self,dx):
        self.prev_val = 0.0
        self.dx = dx
    
    def differentiate(self,data):
        n = data.size
        out = np.zeros(n)
        if abs(self.prev_val) < 1e-6:
            out[0] = 0.0
        else:
            out[0] = (data[0] - self.prev_val)/self.dx

        for i in xrange(1,n):
            out[i] = (data[i] - data[i-1])/self.dx
        
        self.prev_val = data[n-1]
        return out


class central:
    """
    This differentiation method assumes measurements
    are connected through 2nd order polynomials and has an 
    accuracy of O(dx^2).
    """
    def __init__(self,dx):
        self.prev_val = 0.0
        self.fac = 1.0/(2.0*dx)
    
    def differentiate(self,data):
        n = data.size
        out = np.zeros(n)
        if abs(self.prev_val) < 1e-6:
            out[0] = self.fac*(-3.0*data[0]+4.0*data[1]-data[2])
        else:
            out[0] = self.fac*(data[1] - self.prev_val)
                            
        for i in xrange(1,n-1):
            out[i] = self.fac*(data[i+1] - data[i-1])
        
        out[n-1] = self.fac*(data[n-3] - 4.0*data[n-2] + 3.0*data[n-1])
        self.prev_val = data[n-1]
        return out


# ##### Testing
# The same test as for the previous integration part but this time a cosine wave is differentiated. Note the higher accuracy of the central differential quotient.

plt.figure()
f = lambda x: np.cos(x)
f_true = lambda x: -np.sin(x)
ns = 100
x = np.linspace(0,4*np.pi,ns)
dx = np.mean(np.diff(x))
pi2 = 2.0*np.pi
step = 20
start = 0
end = step
o = onesided(dx)
c = central(dx)
while end <= ns:
    x1 = x[start:end]
    y1 = f(x1)
    Yo = o.differentiate(y1)
    Yc = c.differentiate(y1)
    plt.plot(x1/pi2,abs(f_true(x1)-np.array(Yo)),'g-')
    plt.plot(x1/pi2,abs(f_true(x1)-np.array(Yc)),'b-')
    start = end
    end += step
plt.plot(x1/pi2,abs(f_true(x1)-np.array(Yo)),'g-',label='one sided quotient')
plt.plot(x1/pi2,abs(f_true(x1)-np.array(Yc)),'b-',label='central quotient')
plt.legend()
plt.xlabel('# of cycles')
plt.ylabel('Error')
plt.title('Differentiation (synthetic test)')


# Testing with real data by comparing time series from a broad-band sensor and a co-located strong motion sensor. 

from obspy import read
# Load and pre-process the raw data
fin = 'zur.mseed'
hg_gain = 407879.0
hh_gain = 600000000.0
st = read(fin)
acc = st.select(channel='HGZ')[0]
vel = st.select(channel='HHZ')[0]
dt = acc.stats.delta
vel.data = vel.data.astype(float) / hh_gain
acc.data = acc.data.astype(float) / hg_gain
acc.data -= acc.data.mean()
vel.data -= vel.data.mean()

# Compute the derivative of the velocity trace
o = onesided(vel.stats.delta)
c = central(vel.stats.delta)
step = 20*vel.stats.sampling_rate
start = 0
end = step
acc1 = np.zeros(vel.stats.npts)
acc2 = np.zeros(vel.stats.npts)
while True:
    _v = vel.data[start:end]
    acc1[start:end] = o.differentiate(_v)
    acc2[start:end] = c.differentiate(_v)
    start = end
    end += step
    if end >= vel.stats.npts-3:
        end = vel.stats.npts-3
        _v = vel.data[start:end]
        acc1[start:end] = o.differentiate(_v)
        acc2[start:end] = c.differentiate(_v)
        break
        
# Compare the derivative of the velocity trace with 
# the data from the co-located strong motion sensor
fig = plt.figure(figsize=(8,8))
ax1 = fig.add_subplot(2,1,1)
ax2 = fig.add_subplot(2,1,2)
idx0 = int((acc.stats.starttime - vel.stats.starttime)*vel.stats.sampling_rate)
vtimes = vel.times() - vel.times()[idx0]
idxend = min(vel.stats.npts-idx0,acc.stats.npts)
ax1.plot(acc.times(), acc.data,label='True acceleration')
ax1.plot(vtimes,acc1,label='one sided diff')
ax1.plot(vtimes,acc2, label='central diff')
ax1.legend(loc='upper right')
ax1.set_xlim(155,170)
ax2.set_xlabel('Time [s]')
ax2.set_ylabel('Amplitude [m/s/s]')
ax2 = fig.add_subplot(2,1,2)
ax2.plot(vtimes[idx0:idxend+idx0], acc.data[0:idxend]-acc1[idx0:idxend+idx0],'g',label='error one sided diff')
ax2.plot(vtimes[idx0:idxend+idx0], acc.data[0:idxend]-acc2[idx0:idxend+idx0],'r', label='error central diff')
ax2.legend(loc='upper right')
ax2.set_xlim(155,170)
ax2.set_xlabel('Time [s]')
ax2.set_ylabel('Error')
lserror1 = np.sqrt(sum((acc.data[0:idxend]-acc1[idx0:idxend+idx0])**2))
lserror2 = np.sqrt(sum((acc.data[0:idxend]-acc2[idx0:idxend+idx0])**2))
print "Least squares error for one sided differentiation is: %f" %lserror1
print "Least squares error for central differentiation is: %f" %lserror2
ax1.set_title('Differentiation (real data test)')



# $\tau_p$

def taup(x,sps):
    """
    Implementation of the taup algorithm.
    x = velocity time series
    sps = sampling frequency
    """
    tp = np.zeros(x.size)
    dt = 1.0/sps
    x_old = x[1]**2
    d_old = ((x[1] - x[0])/dt)**2
    alpha = 1.0-1.0/sps
    for i in range(2,x.size):
        D = alpha*d_old + ((x[i] - x[i-1])/dt)**2
        d_old = D
        X = alpha*x_old + x[i]**2
        x_old = X
        tp[i] = 2*np.pi*np.sqrt(X/D)
    return tp


# Testing $\tau_p$ with a synthetic signal consisting of 3
# different frequencies, amplitudes, and phases
sps = 100 # Sampling rate = 100 Hz
t = np.arange(0,5,1.0/sps) # Length of the signal = 5 s
f1 = 10; f2 = 20; f3 = 30 # frequencies
phi1 = np.pi/2; phi2 = np.pi/8.; phi3 = np.pi/4 # phases
a1 = 20; a2 = 2; a3 = .2 # amplitudes
s1 = a1*np.sin(2*np.pi*f1*t+phi1)
s2 = a2*np.sin(2*np.pi*f2*t+phi2)
s3 = a3*np.sin(2*np.pi*f3*t+phi3)
s = s1+s2+s3

# Computing $\tau_p$:
tp = taup(s,sps)
print "Mean of taup: %.2f" % tp.mean()
print "Dominant period: %.2f" % (1.0/f1)

# Plotting the results:
fig = plt.figure()
ax1 = fig.add_subplot(2,1,1)
ax1.plot(t,s)
ax1.set_xlim(0,5)
ax2 = fig.add_subplot(2,1,2)
ax2.plot(t,tp)
ax2.hlines(1.0/f1,0,10)
ax2.set_xlim(0,4)
ymin, ymax = ax2.get_ylim()
if ymax <= 1.0/f1:
    ymax = 1.0/f1 + 0.2*(1.0/f1)
ax2.set_ylim(ymin,ymax)
ax2.set_ylabel(r'$\tau_p [s]$')
ax2.set_xlabel("Time [s]")
ax1.set_title(r'$\tau_p$')


# #### References
# Allen, R. M., and H. Kanamori (2003), The potential for earthquake early warning in southern California, Science, 300, 786–789.
# 
# Olson, E. L., and R. M. Allen (2005), The deterministic nature of earthquake rupture., Nature, 438(7065), 212–5, doi:10.1038/nature04214.
# 
# Nakamura, Y. (1988), On the Urgent Earthquake Detection and Alarm System (UrEDAS), in Proceedings of Ninth World Conference on Earthquake Engineering, Tokyo-Kyoto, Japan.


# $\tau_c$ and $P_d$

def tauc_Pd(vel,disp,pick,sps,tau0=3):
    """
    Returns the tau_c and Pd values for a given
    time window.
    vel = velocity time series
    disp = displacement time series
    pick = P-wave detection time
    sps = sampling frequency
    tau0 = evaluation window
    """
    dt = 1.0/sps
    start = pick*sps
    end = tau0*sps
    ct1 = cumtrapz(dt)
    ct2 = cumtrapz(dt)
    r_1      = ct1.integrate(vel[start:(end+1)]**2)
    r_2      = ct2.integrate(disp[start:(end+1)]**2)
    r        = r_1[-1]/r_2[-1]
    return (2*np.pi/np.sqrt(r), abs(disp[start:(end+1)]).max())


# Testing $\tau_c$ with the same synthetic signal as $\tau_p$
vel = s
cs = cumtrapz(1.0/sps)
disp = cs.integrate(vel)

# Computing $\tau_c$
tc,pd = tauc_Pd(vel,disp,0,sps,tau0=3)
print "Value of tau_c: %.2f; Value of P_d: %.2f" % (tc,pd)
print "Dominant period: %.2f" % (1.0/f1)


# #### References
# Kanamori, H. (2005), Real-Time Seismology and Earthquake Damage Mitigation, Annu. Rev. Earth Planet. Sci., 33(1), 195–214, doi:10.1146/annurev.earth.33.092203.122626.
# 
# Satriano, C., Y.-M. Wu, A. Zollo, and H. Kanamori (2011), Earthquake early warning: Concepts, methods and physical grounds, Soil Dyn. Earthq. Eng., 31(2), 106–118, doi:10.1016/j.soildyn.2010.07.007.

plt.show()


