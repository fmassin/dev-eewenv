# sceewamps: Amplitudes for EEW algorithms in SeisComP3 
Yannik Behr, Maren Böse, John Clinton, Men-Andrin Meier

Files:
- PDF: specification annotated by Fred Massin while testing implementation,
- ipynb: demonstration of the algorithm,
- html: special demonstration of the GBA  algorithm,
- py: implementation examples,
- mseed: data example,
- png, eps: figures.
